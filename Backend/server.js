const express =require("express");
const mysql=require("mysql");
const cors = require('cors');



const app=express();

app.use(express.json());

const corsOptions = {
    origin: '*', // Allow all origins (change to a specific origin as needed)
    methods: 'GET, POST, PUT, DELETE', // Specify allowed methods
    allowedHeaders: 'Content-Type, Authorization', // Specify allowed headers
};

// Use the `cors` middleware
app.use(cors(corsOptions));


let connectionPool;

app.post("/api",(req,res)=>{

    //const {username, password}=req.body;

    connectionPool=mysql.createPool({
        connectionLimit:10,
        host:"localhost",
        user:`${req.body.username}`,
        password:`${req.body.password}`
    })

    console.log("MySQL connection pool setup successfully");
    
    connectionPool.getConnection((err,connection)=>{
        if(err){
            console.error('Error acquiring connection from pool:', err);
            res.status(500).send('Error acquiring connection from pool');
            return; 
        }

        const createDB="create database IF NOT EXISTS AmanDB";

        connection.query(createDB,(error,result)=>{
            if(error){
                console.log("Error in creating DB")
            }else{
                res.status(201).json({"database":"amandb"})
                connection.release();
            }
            
        })
    })

})

app.post("/api/createEntity",(req,res)=>{

    connectionPool.getConnection((err,connection)=>{
        if(err){
            console.error('Error acquiring connection from pool:', err);
            res.status(500).send('Error acquiring connection from pool');
            return; 
        }

    
        connection.query(`USE ${req.body.databaseName}`, (selectError) => {
            if (selectError) {
              console.error('Error selecting database:', selectError);
              res.status(500).send('Error selecting database');
              return;
            }
    

            let columns=req.body.column;

            let createTableQuery = `CREATE TABLE IF NOT EXISTS ${req.body.entity} (`;

            createTableQuery+="id INT AUTO_INCREMENT PRIMARY KEY,"

            // Add columns dynamically
            columns.forEach((column, index) => {
                createTableQuery += `${column.name} ${column.type}`;

                // Add a comma if it's not the last column
                if (index < columns.length - 1) {
                    createTableQuery += ', ';
                }
            });

            createTableQuery += ')';

            connection.query(createTableQuery,(err,results,fields)=>{
                if(err){
                    console.error('Error creating table:', err);
                }else{
                    console.log('Table created successfully!');
                    connection.query(`describe ${req.body.entity}`,(error,results)=>{
                        if(error){
                            console.log("Error in describing structure of table")
                        }else{
                            res.status(201).json({"entity":`${req.body.entity}`,results})
                            console.log("Structure of table fetched")
                        }
                    })

                    //res.status(201).json({"entity":`${req.body.entity}`})
                    connection.release();
                }
            })
        })
        
    })
})

app.post("/api/insert",(req,res)=>{
    connectionPool.getConnection((err,connection)=>{
        if(err){
            console.error('Error acquiring connection from pool:', err);
            res.status(500).send('Error acquiring connection from pool');
            return; 
        }

        connection.query(`USE ${req.body.databaseName}`, (selectError) => {
            if (selectError) {
              console.error('Error selecting database:', selectError);
              res.status(500).send('Error selecting database');
              return;
            }
            const {entity,data}=req.body;

            const keys=Object.keys(data);
            const values=Object.values(data);

            console.log('Keys:', keys);
            console.log('Values:', values);

            // Construct the dynamic insert query
            const insertQuery = `INSERT INTO ${entity} (${keys.map(key => mysql.escapeId(key)).join(', ')}) VALUES (${values.map(value => mysql.escape(value)).join(', ')})`;

            
                connection.query(insertQuery,(err,results,fields)=>{
                    if(err){
                        console.error('Error inserting into table:', err);
                    }else{
                        console.log("1 row inserted sucessfully");
                        res.status(200).json({message:"1 row inserted successfully"});
                        connection.release();
                    }
                }) 
            

        })

    })
})


app.put("/api/update/:id",(req,res)=>{

    console.log("id: "+req.params.id)
    connectionPool.getConnection((err,connection)=>{
        if(err){
            console.error('Error acquiring connection from pool:', err);
            res.status(500).send('Error acquiring connection from pool');
            return; 
        }

        connection.query(`USE ${req.body.databaseName}`, (selectError) => {
            if (selectError) {
              console.error('Error selecting database:', selectError);
              res.status(500).send('Error selecting database');
              return;
            }
            const {entity,data}=req.body;

            const keys=Object.keys(data);
            const values=Object.values(data);

            console.log('Keys:', keys);
            console.log('Values:', values);

            //Construct the dynamic update query
            const updateQuery = `UPDATE ${entity} SET ${keys.map(key => `${mysql.escapeId(key)} = ${mysql.escape(data[key])}`).join(', ')} WHERE id = ${req.params.id}`;

            
                connection.query(updateQuery,(err,results,fields)=>{
                    if(err){
                        console.error('Error updating into table:', err);
                    }else{
                        console.log("1 row updated sucessfully");
                        res.status(200).json({message:"1 row updated successfully"});
                        connection.release();
                    }
                }) 
            

        })

    })
})

app.delete("/api/delete/:id",(req,res)=>{

    console.log("id: "+req.params.id)
    //console.log("database: "+req.query.data.databaseName)
    console.log("database: "+req.query.databaseName)
    connectionPool.getConnection((err,connection)=>{
        if(err){
            console.error('Error acquiring connection from pool:', err);
            res.status(500).send('Error acquiring connection from pool');
            return; 
        }

        connection.query(`USE ${req.query.databaseName}`, (selectError) => {
            if (selectError) {
              console.error('Error selecting database:', selectError);
              res.status(500).send('Error selecting database');
              return;
            }
            const {entity}=req.query;

            //Construct the dynamic update query
            const deleteQuery = `DELETE FROM ${entity} WHERE id = ${req.params.id}`;

            
                connection.query(deleteQuery,(err,results,fields)=>{
                    if(err){
                        console.error('Error deleting into table:', err);
                    }else{
                        console.log("1 row deleted sucessfully");
                        res.status(200).json({message:"1 row deleted successfully"});
                        connection.release();
                    }
                }) 
            

        })

    })
})

app.get("/api/show",(req,res)=>{

    console.log("database: "+req.query.databaseName)
    connectionPool.getConnection((err,connection)=>{
        if(err){
            console.error('Error acquiring connection from pool:', err);
            res.status(500).send('Error acquiring connection from pool');
            return; 
        }

        connection.query(`USE ${req.query.databaseName}`, (selectError) => {
            if (selectError) {
              console.error('Error selecting database:', selectError);
              res.status(500).send('Error selecting database');
              return;
            }
            //const {entity}=req.params;

            //Construct the dynamic update query
            const selectQuery = `SELECT * FROM ${req.query.entity}`;

            
                connection.query(selectQuery,(err,results,fields)=>{
                    if(err){
                        console.error('Error getting table:details', err);
                    }else{
                        console.log("Table fetched successfully");
                        res.status(200).json({results});
                        connection.release();
                    }
                }) 
            

        })

    })
})

const Port=5000;


app.listen(Port,()=>{
    console.log(`Server is listening to port ${Port}`)
})