import React, { useRef, useState } from "react";

import { Link, useNavigate } from "react-router-dom";
import axios from "axios";

import './login.css';
import photo from "../photos/databasePhoto.jpg"


export default function Login(props) {
  const navigate=useNavigate();
  
  const form = useRef();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState("");





  const onChangeUsername = (e) => {
    let username = e.target.value;
    setUsername(username);
  };
  const onChangePassword = (e) => {
    let password = e.target.value;
    setPassword(password);
  };

  
  const handleLogin = async(e) => {
    e.preventDefault();
    setMessage("");
    setLoading(true);

    const payload={
        username:username,
        password:password
    }
    
    const result=await axios.post("http://localhost:5000/api",payload)
                        .then((response)=>{
                            //console.log(response.data.database)
                            localStorage.setItem("database",response.data.database)
                            navigate("/home")
                        }).catch((err)=>{
                          console.log(err);
                        })
    
      
  };

  return (
    <div>
      
      <img src={photo} className="card-img-top rounded-3 " alt="..." style={{width:"1620px",height:"750px",marginTop:"0px",marginLeft:"0px"}}/>

      
      <form className="login" onSubmit={handleLogin} ref={form}>
        <h3>
          <b>MySQL Credentials to connect with Database</b>
        </h3>

        <div className="form-outline mb-2">
          <label className="form-label">
            Username
          </label>
          <input
            type="text"
            id="form2"
            className="form-control"
            name="username"
            placeholder="Enter your Username"
            value={username}
            required
            onChange={onChangeUsername}
          />
        </div>

        <div className="form-outline mb-2">
          <label className="form-label">
            Password
          </label>
          <input type="password" id="form23" className="form-control" name="password" value={password} required placeholder="Enter your Password" onChange={onChangePassword} />
        </div>

        <div className="text-center pt-1 mb-2 pb-1" >
          <button className="btn btn-success btn-block fa-lg mb-3">
            
            <span>Connect</span>
          </button>
          {message && (
            <div className="form-group">
              <div className="alert alert-danger" role="alert">
                {message}
              </div>
            </div>
          )}
          

        </div>

        {/* <div className="d-flex align-items-center justify-content-center pb-2">
          <p className="mb-2 me-2" style={{color:"blue" }}>Don't have an account?</p>
          <button type="button" className="btn btn-outline-danger" style={{backgroundColor:"white"}}>
            <Link to="/Register" style={{color:"blue", }}>Create new</Link>
          </button>
        </div> */}
      </form>
    </div>
  );
}