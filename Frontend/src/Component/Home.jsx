import Header from "./Header";
import photo from "../photos/WhitePhoto.jpg"

function Home() {
    return (
        <>
            <Header/>
            {/* <img src={photo} className="card-img-top rounded-3" alt="..." style={{width:"1500px",height:"700px",marginTop:"50px",marginLeft:"1px"}}/>
            <h1>Welcome to Abstract MySQL</h1> */}
            <div style={{ position: "relative", width: "1500px", height: "700px" }}>
                <img src={photo} className="card-img-top rounded-3" alt="..." style={{ width: "100%", height: "100%" }} />
                <h1 style={{ position: "absolute", top: "50%", left: "50%", transform: "translate(-50%, -50%)",color:"darkolivegreen" }}><b>Welcome to Abstract MySQL</b></h1>
            </div>
        </>
      );
}

export default Home;