import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Header from './Header';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';

function ShowTable() {
  const [data, setData] = useState([]);
  const [columns, setColumns] = useState([]);

  useEffect(() => {
    // Retrieve databaseName and entity from local storage
    const databaseName = localStorage.getItem('database');
    const entity = localStorage.getItem('entity');

    // Fetch data from the API
    const fetchData = async () => {
      try {
        const payload={
            databaseName:databaseName,
            entity:entity
        }
        const response = await axios.get("http://localhost:5000/api/show",{
            params: payload
          });
        const results = response.data.results;

        if (results.length > 0) {
          // Set columns dynamically based on keys of the first object
          setColumns(Object.keys(results[0]));
        }

        setData(results);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []);

  const deleteItem= async (itemId)=>{
    try {
        const databaseName=localStorage.getItem('database');
        const entity=localStorage.getItem('entity')

        const payload={
            databaseName:databaseName,
            entity:entity
        }
         const response = await axios.delete(`http://localhost:5000/api/delete/${itemId}`,{params:payload});
     
         if (response.status === 200) {
           // Category deleted successfully
           console.log(`Item with ID ${itemId} has been deleted`);
         } 
         else {
           // Category deletion failed
           console.error(`Failed to delete item with ID ${itemId}`);
         }
         window.location.reload();
       } 
       catch (error) {
         console.error(`An error occurred while deleting item with ID ${itemId}: ${error}`);
       }
   }

  return (
    
    <>
        <div><Header/></div>
        <div>
      <h1>Data from {localStorage.getItem('database')} - {localStorage.getItem('entity')}</h1>
      <div className="container-fluid " style={{ width: '1500px', border:'1px solid black' }}>
        <div className="row fw-bold col-md-12" style={{ border:'1px solid black' }}>
          {columns.map((column, index) => (
            <div className="col-md-2" key={index}>
              <strong>{column}</strong>
            </div>
          ))}
          <div className="col-md-2"><strong>Action</strong></div>
        </div>
        {data.map((row, rowIndex) => (
          <div className="row py-2" key={rowIndex}>
            {columns.map((column, colIndex) => (
              <div className="col-md-2" key={colIndex}>
                {row[column]}

                
              </div>
                

            ))}
            <div className='col-md-2'>
                <Link to={`/updateTable/${row.id}`} state={{data:row}} >
                      <Button color="warning">Update</Button>
                </Link>
                        <span>   </span>
                        
                <Button color="danger" onClick={()=>deleteItem(row.id)}>Delete</Button>
            </div>
          </div>
        ))}
      </div>
    </div>
    </>
    
  );
}

export default ShowTable;

// {/* <>
//     <div><Header/></div>
    
//     <div className="container-fluid " style={{ width: '1500px', border:'1px solid black' }} >
//             {/* Header row */}
//             <div className="row fw-bold col-md-12" style={{ border:'1px solid black' }}>
                

//                         {columns.map((column, index) => (
//                             <div className="col-md-1" key={index}>{column}</div>
//                         ))}
                
                
//             </div>
            
//             {/* Data rows */}
//             <div className="row py-2">
//             {data.map((row, rowIndex) => (

//             ))}
//             </div>
//             {employees.map((item) => (
//                 <div className="row py-2" key={item._id}>
//                     <div className="col-md-1">{item.id}</div>
//                     <div className="col-md-1">
                        
//                         <img
//                             src={`../../public/images/${item.image}`}
//                             alt={`${item.name}`}
//                             className="img-thumbnail"
//                             style={{ height: '100px', width: '100px' }}
//                         />
//                     </div>
//                     <div className="col-md-2">{item.name}</div>
//                     <div className="col-md-2">{item.email}</div>
//                     <div className="col-md-1">{item.mobileNo}</div>
//                     <div className="col-md-1">{item.course}</div>
//                     <div className="col-md-1">{item.designation}</div>
//                     <div className="col-md-1">{item.gender}</div>
//                     <div className="col-md-2">
//                     {/* itemId={item} */}

                    





//           <Link to={`/updateEmployee/${item._id}`} state={{data:item}} >
//                       <Button color="warning">Update</Button>
//           </Link>
//                         <span>   </span>
                        
//                         <Button color="danger" onClick={()=>deleteItem(item._id)}>Delete</Button>

//                     </div>
//                 </div>
//             ))}
//         </div>
//     </> */}