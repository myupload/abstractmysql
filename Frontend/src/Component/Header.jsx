import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useNavigate, useLocation } from 'react-router-dom';
//import swal from "sweetalert";
import { useState, useEffect } from 'react';

export default function Header() {

    const database=localStorage.getItem("database");
    const navigate = useNavigate();
    const location = useLocation();
   // const [isLoggedIn, setIsLoggedIn] = useState(false);

    // useEffect(() => {
    //     setIsLoggedIn(localStorage.getItem("isLoggedIn") === "true");
    //   }, [location]);

    // function handleLogin() {
    
    //     if (isLoggedIn) {
    //       localStorage.clear();
    //       setIsLoggedIn(false);
    //       swal("successfully Logged Out, Redirecting to Login page")
    //       navigate('/')
    //     } else {
            
    //       navigate('/login');
          
    //     }
    //   }

    return (
        <header>
            <nav className="navbar navbar-expand-lg navbar-light bg-white shadow fixed-top">
                <div className="container-fluid">
                
                    <Link to="/" className="navbar-brand d-flex align-items-center">
                        <img
                            src="https://alexharkness.com/wp-content/uploads/2020/06/logo-2.png"
                            alt="Logo"
                            className="me-3"
                            style={{ height: '48px' }}
                        />
                    </Link>
                    
                    <button
                        className="navbar-toggler"
                        type="button"
                        data-bs-toggle="collapse"
                        data-bs-target="#navbarNav"
                        aria-controls="navbarNav"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                    >
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav me-auto">
                            <li className="nav-item">
                                <NavLink
                                    to="/home"
                                    className={({ isActive }) =>
                                        `nav-link ${isActive ? 'text-primary' : ''}`
                                    }
                                >
                                    Home
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink
                                    to="/showTable"
                                    className={({ isActive }) =>
                                        `nav-link ${isActive ? 'text-primary' : ''}`
                                    }
                                >
                                    Show Table
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink
                                    to="/createTable"
                                    className={({ isActive }) =>
                                        `nav-link ${isActive ? 'text-primary' : ''}`
                                    }
                                >
                                    Create Table
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink
                                    to="/insertTable"
                                    className={({ isActive }) =>
                                        `nav-link ${isActive ? 'text-primary' : ''}`
                                    }
                                >
                                    Insert into Table
                                </NavLink>
                            </li>
                        </ul>
                        
                        
                    </div>
                </div>
            </nav>
        </header>
    );
}