import photo from "../photos/WhitePhoto.jpg"
import './login.css';
import React, { useState } from 'react';
import axios from 'axios';
import Header from "./Header";
import { useNavigate } from 'react-router-dom';

function CreateTable() {
  const [columns, setColumns] = useState([
    { name: '', type: '' },  
  ]);
  const [entity,setEntity]=useState("");

  const navigate=useNavigate();

  const onChangeEntity=(e)=>{
    setEntity(e.target.value);
  }

  const handleAddColumn = () => {
    setColumns([...columns, { name: '', type: '' }]);
  };

  const handleChange = (index, event) => {
    const updatedColumns = columns.map((column, colIndex) => {
      if (colIndex === index) {
        return { ...column, [event.target.name]: event.target.value };
      }
      return column;
    });
    setColumns(updatedColumns);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const database=localStorage.getItem("database")
    const payload = {
        databaseName:database,
        entity: entity,
        column: columns,
    };

    try {
      const response = await axios.post('http://localhost:5000/api/createEntity', payload).then((response)=>{
        console.log('Response:', response.data);
        localStorage.setItem("entity",response.data.entity)
        localStorage.setItem("tableStructure",JSON.stringify(response.data.results))
        navigate("/insertTable")
      }).catch((err)=>{
        console.log(err)
      })
      
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <div className="App">
        <Header/>
        <img src={photo} className="card-img-top rounded-3" alt="..." style={{width:"1500px",height:"700px",marginTop:"50px",marginLeft:"1px"}}/>
      <form className="register" onSubmit={handleSubmit}>
            <h3>
            <b>Create Table</b>
          </h3>
          <h6>Note: Id column will automatically generate with Primary key attribute</h6>
        <div className="form-outline mb-2">
            <label className="form-label">
              Entity Name
            </label>
            <input
              type="text"
              id="form2"
              className="form-control"
              name="entity"
              placeholder="Enter Entity Name"
              value={entity}
              required
              onChange={onChangeEntity}
            />
        </div>
        <label className="form-label">
              Column Name and Datatype
        </label>

        {columns.map((column, index) => (
          <div key={index} className="input-group">
            <input
              type="text"
              name="name"
              className="form-control"
              value={column.name}
              placeholder="Column Name"
              onChange={(e) => handleChange(index, e)}
            />
            <input
              type="text"
              name="type"
              className="form-control"
              value={column.type}
              placeholder="Type: e.g. (varchar(20) UNIQUE)"
              onChange={(e) => handleChange(index, e)}
            />
          </div>
        ))}
        <button type="button" onClick={handleAddColumn} className="btn btn-primary btn-block fa-lg mb-3">
          Add Column
        </button>
        <button type="submit" className="btn btn-success btn-block fa-lg mb-3">Submit</button>
      </form>
    </div>
  );
}

export default CreateTable;


//===================================================================================================================

// function CreateTable() {
//     return ( <>
//     <div>
//         {/* <img src={photo} className="card-img-top rounded-3 fullscreen-image" alt="..." /> */}
//         <img src={photo} className="card-img-top rounded-3 " alt="..." style={{width:"1700px",height:"780px"}}/>
//         <form className="register" onSubmit={handleRegister} ref={form}>
//           <h3>
//             <b>Create Entity</b>
//           </h3>
  
        //   <div className="form-outline mb-2">
        //     <label className="form-label">
        //       Name
        //     </label>
        //     <input
        //       type="text"
        //       id="form2"
        //       className="form-control"
        //       name="Name"
        //       placeholder="Enter your Name"
        //       value={name}
        //       required
        //       onChange={onChangeName}
        //     />
        //   </div>
          
//           <div className="form-outline mb-2">
//             <label className="form-label">
//               Email
//             </label>
//             <input
//               type="email"
//               id="form4"
//               className="form-control"
//               name="email"
//               placeholder="Enter your Email"
//               required
//               value={email}
//               onChange={onChangeEmail}
//             />
//           </div>
//           <div className="form-outline mb-2">
//             <label className="form-label">
//               Password
//             </label>
//             <input type="password" id="form23" className="form-control" name="password" value={password} required placeholder="Enter your Password" onChange={onChangePassword} />
//           </div>
//           <div className="form-outline mb-2">
//             <label className="form-label">
//               Confirm Password
//             </label>
//             <input
//               type="password"
//               id="form24"
//               className="form-control"
//               name="confirmPassword"
//               placeholder="Re-Enter Password"
//               required
//               value={confirmpassword}
//               onChange={onChangeConfirmPassword}
//             />
//           </div>
          
        
//           <div className="text-center pt-1 mb-2 pb-1">
//             <button className="btn btn-success btn-block fa-lg mb-3" /*disabled={loading}*/>
//               {/* {loading && (
//                 <span className="spinner-border spinner-border-sm"></span>
//               )} */}
//               <span>Register</span>
//             </button>
//             {message && (
//               <div className="form-group">
//                 <div className="alert alert-danger" role="alert">
//                   {message}
//                 </div>
//               </div>
//             )}
//             {/* <button class="btn btn-success btn-block fa-lg mb-3" type="button">
//               Log in
//             </button> */}
//           </div>
//         </form>
//       </div>
//     </> );
// }

// export default CreateTable;