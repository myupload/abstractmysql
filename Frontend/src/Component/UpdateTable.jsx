import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Header from './Header';
import photo from "../photos/WhitePhoto.jpg"
import './login.css';
import swal from 'sweetalert';
import { useLocation } from 'react-router-dom';

function UpdateTable() {
  const [entity, setEntity] = useState('');
  const [tableStructure, setTableStructure] = useState([]);
  const [formData, setFormData] = useState({});

  const location = useLocation();

    const data = location.state?.data;
    console.log(data)

  useEffect(() => {
    // Retrieve entity and table structure from local storage
    const entity = localStorage.getItem('entity');
    const tableStructure = JSON.parse(localStorage.getItem('tableStructure'));
    

    setEntity(entity);
    setTableStructure(tableStructure);

    // Initialize form data
    const initialFormData = {};
    tableStructure.forEach(field => {
      //initialFormData[field.Field] = '';
      if (field.Field !== 'id') {
        initialFormData[field.Field] = data[field.Field];
      }
    });
    setFormData(initialFormData);
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const databaseName=localStorage.getItem('database');

    // Send form data to the server
    const payload={
        databaseName:databaseName,
        entity:entity,
        data:formData
    }
    try {
      const response = await axios.put(`http://localhost:5000/api/update/${data.id}`, payload);
      console.log('Response:', response.data);
      swal(response.data.message);

    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <div>
      <Header/>
        <img src={photo} className="card-img-top rounded-3" alt="..." style={{width:"1500px",height:"700px",marginTop:"50px",marginLeft:"1px"}}/>
      <form className="register" onSubmit={handleSubmit}>
            <h3>
                <b>Update into {entity}</b>
            </h3>
        {tableStructure.filter(field => field.Field !== 'id').map((field, index) => (
          <div key={index}>
            <label>{field.Field}</label>
            
            <input
              type="text"
              name={field.Field}
              value={formData[field.Field]}
              className="form-control"
              onChange={handleChange}
              required={field.Null === 'NO'}
            />
          </div>
        ))}
        <button type="submit" className="btn btn-success btn-block fa-lg mb-3">Update</button>
      </form>
    </div>
  );
}

export default UpdateTable;