import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Login from './Component/Login';
import Home from './Component/Home';
import CreateTable from './Component/CreateTable';
import InsertTable from './Component/InsertTable';
import ShowTable from './Component/ShowTable';
import UpdateTable from './Component/UpdateTable';

function App() {
  

  return (
    <>
      <BrowserRouter>
        <Routes>
            <Route path="/" element={<Login/>} />
            <Route path="/home" element={<Home/>} />
            <Route path="/createTable" element={<CreateTable/>} />
            <Route path="/insertTable" element={<InsertTable/>} />
            <Route path="/updateTable/:id" element={<UpdateTable/>} />
            <Route path="/showTable" element={<ShowTable/>} />
        </Routes>
    </BrowserRouter>
    </>
  )
}

export default App
